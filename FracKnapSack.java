FracKnapSack.java

Type
Java
Size
1 KB (1,186 bytes)
Storage used
1 KB (1,186 bytes)
Location
Algorithm Lab
Owner
me
Modified
Jul 12, 2017 by me
Opened
8:17 AM by me
Created
Jul 12, 2017 with Google Drive Web
Add a description
Viewers can download
import java.util.Scanner;

public class FracKnapSack {
	public static void main(String args[]){
		int i, j=0, knapsack_weight, m,n;
		float sum=0, max;
		Scanner in = new Scanner(System.in);
		int arr[][] = new int[2][20];
		System.out.println("Enter no. of items: ");
		n = in.nextInt();
		System.out.println("Enter the weights of the items: ");
		for(i=0;i<n;++i)
			arr[0][i] = in.nextInt();
		System.out.println("Enter the values of the items: ");
		for(i=0;i<n;++i)
			arr[1][i] = in.nextInt();
		System.out.println("Enter Knapsack weight: ");
		knapsack_weight = in.nextInt();
		m = knapsack_weight;
		while(m>=0){
			max=0;
			for(i=0;i<n;++i){
				if((float)arr[1][i]/(float)arr[0][i] > max){
					max = (float)arr[1][i]/(float)arr[0][i];
					j=i;
				}
			}
			if(arr[0][j] > m){
				System.out.println("Quantity of each item: " + (j+1) + " added is " + m);
				sum += m*max;
				m = -1;
			}
			else{
				System.out.println("Quantity of each item: " + (j+1) + " added is " + arr[0][j]);
				m -= arr[0][j];
				sum += (float)arr[1][j];
				arr[1][j]=0;
			}
		}
		System.out.println("Maximum profit is: " + sum);
		in.close();
	}
}